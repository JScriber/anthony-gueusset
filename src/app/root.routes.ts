import { Routes } from '@angular/router';

// Pages.
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';

// Sub-routes definitions.
import { projectsRoutes } from './pages/projects/projects.routes';
import { skillsRoutes } from './pages/skills/skills.routes';

/** Routes definition. */
export const routes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    data: {
      animation: 'dashboard'
    }
  },
  {
    path: 'projects',
    children: projectsRoutes,
  },
  {
    path: 'skills',
    children: skillsRoutes
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];
