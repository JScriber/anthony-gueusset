import { Component } from '@angular/core';

// Services.
import { MenuManagerService } from '../../layouts/menu-manager/menu-manager.service';

export interface ListItem {

  /** Unique ID. */
  id: number;

  /** Logo. */
  logo?: string;

  /** Name. */
  name: string;

  /** Short description. */
  description: string;

}

interface Enterprise {

  status: string;

  date: string;

  enterpriseLogo: string;

  enterpriseName: string;

}

/**
 * Global overview of all the informations.
 */
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {

  skills: ListItem[] = [
    {
      id: 0,
      name: 'Angular',
      description: 'TypeScript',
      logo: '../../../assets/images/icons/mock/angular.svg'
    },
    {
      id: 1,
      name: 'Spring boot',
      description: 'Java / JEE',
      logo: '../../../assets/images/icons/mock/springb.svg'
    },
    {
      id: 2,
      name: 'NestJS',
      description: 'Node.js',
      logo: '../../../assets/images/icons/mock/nest.svg'
    }
  ];

  projects: ListItem[] = [
    {
      id: 0,
      name: 'Portfolio',
      description: 'Octobre 2019'
    },
    {
      id: 1,
      name: 'Gameboy Pokémon',
      description: 'Septembre 2019',
      logo: '../../../assets/images/icons/mock/pokemon.svg'
    },
    {
      id: 2,
      name: 'Labyrinthe 3D',
      description: 'Août 2019',
      logo: '../../../assets/images/icons/mock/maze.svg'
    }
  ];

  enterprises: Enterprise[] = [
    {
      status: 'Alternant - Dev. Full-stack',
      date: '2018 - Aujourd\'hui',
      enterpriseLogo: '../../../assets/images/enterprises/tactfactory.jpg',
      enterpriseName: 'TACTfactory'
    },
    {
      status: 'Stagiaire',
      date: '2018 - 5 mois',
      enterpriseLogo: '../../../assets/images/enterprises/tactfactory.jpg',
      enterpriseName: 'TACTfactory'
    }
  ];

  constructor(private readonly menu: MenuManagerService)  {}

  /** Opens up the menu. */
  openMenu() {
    this.menu.changeState(true);
  }

}
