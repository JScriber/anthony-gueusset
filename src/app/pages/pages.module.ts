import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// Angular material.
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatRippleModule } from '@angular/material/core';

// Pages.
import { DashboardComponent } from './dashboard/dashboard.component';
import { NotFoundComponent } from './not-found/not-found.component';

// Modules.
import { ProjectsModule } from './projects/projects.module';
import { SkillsModule } from './skills/skills.module';
import { LayoutsModule } from '../layouts/layouts.module';

@NgModule({
  imports: [
    RouterModule,
    MatIconModule,
    MatButtonModule,
    MatRippleModule,

    CommonModule,
    LayoutsModule,
    ProjectsModule,
    SkillsModule
  ],
  declarations: [
    DashboardComponent,
    NotFoundComponent
  ]
})
export class PagesModule {}
