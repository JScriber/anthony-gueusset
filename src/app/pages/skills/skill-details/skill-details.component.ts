import { Component } from '@angular/core';

/**
 * Informations on a specific skill.
 */
@Component({
  selector: 'app-skill-details',
  templateUrl: './skill-details.component.html',
  styleUrls: ['./skill-details.component.scss']
})
export class SkillDetailsComponent {}
