import { Component } from '@angular/core';
import { ListItem } from '../../dashboard/dashboard.component';

/**
 * List of all the skills.
 */
@Component({
  selector: 'app-skill-list',
  templateUrl: './skill-list.component.html',
  styleUrls: ['./skill-list.component.scss']
})
export class SkillListComponent {

  skills: ListItem[] = [
    {
      id: 1,
      name: 'Angular',
      description: 'TypeScript',
      logo: '../../../assets/images/icons/mock/angular.svg'
    },
    {
      id: 1,
      name: 'Spring boot',
      description: 'Java / JEE',
      logo: '../../../assets/images/icons/mock/springb.svg'
    },
    {
      id: 2,
      name: 'NestJS',
      description: 'Node.js',
      logo: '../../../assets/images/icons/mock/nest.svg'
    },
    {
      id: 3,
      name: 'React.js',
      description: 'JavaScript',
      logo: '../../../assets/images/icons/mock/react.svg'
    }
  ];
}
