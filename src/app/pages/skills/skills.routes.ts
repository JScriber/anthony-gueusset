import { Routes } from '@angular/router';

// Pages.
import { SkillListComponent } from './skill-list/skill-list.component';
import { SkillDetailsComponent } from './skill-details/skill-details.component';

/** Skills routes definition. */
export const skillsRoutes: Routes = [
  {
    path: '',
    component: SkillListComponent,
    data: {
      animation: 'list'
    }
  },
  {
    path: ':id',
    component: SkillDetailsComponent,
    data: {
      animation: 'details'
    }
  }
];
