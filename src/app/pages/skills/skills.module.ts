import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// Angular material.
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatRippleModule } from '@angular/material/core';

// Components.
import { SkillListComponent } from './skill-list/skill-list.component';
import { SkillDetailsComponent } from './skill-details/skill-details.component';

// Modules.
import { SharedModule } from '../../shared/shared.module';
import { LayoutsModule } from '../../layouts/layouts.module';

/**
 * Module related to the skills.
 */
@NgModule({
  imports: [
    CommonModule,
    RouterModule,

    MatButtonModule,
    MatExpansionModule,
    MatIconModule,
    MatRippleModule,

    LayoutsModule,
    SharedModule
  ],
  declarations: [
    SkillListComponent,
    SkillDetailsComponent
  ]
})
export class SkillsModule {}
