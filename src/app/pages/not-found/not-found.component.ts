import { Component } from '@angular/core';

/**
 * The page asked by the client isn't available or doesn't exist.
 */
@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent {}
