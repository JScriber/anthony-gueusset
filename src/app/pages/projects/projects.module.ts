import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// Angular material.
import { MatTabsModule } from '@angular/material/tabs';
import { MatButtonModule } from '@angular/material/button';
import { MatRippleModule } from '@angular/material/core';

// Components.
import { ProjectDetailsComponent } from './project-details/project-details.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { LayoutsModule } from '../../layouts/layouts.module';
import { MatIconModule } from '@angular/material/icon';

/**
 * Module related to the projects.
 */
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MatTabsModule,
    MatButtonModule,
    MatRippleModule,
    MatIconModule,

    LayoutsModule
  ],
  declarations: [
    ProjectDetailsComponent,
    ProjectListComponent
  ]
})
export class ProjectsModule {}
