import { Component } from '@angular/core';

/**
 * Informations on a specific project.
 */
@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.scss']
})
export class ProjectDetailsComponent {}
