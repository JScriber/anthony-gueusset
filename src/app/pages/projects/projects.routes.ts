import { Routes } from '@angular/router';

// Pages.
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectDetailsComponent } from './project-details/project-details.component';

/** Projects routes definition. */
export const projectsRoutes: Routes = [
  {
    path: '',
    component: ProjectListComponent,
    data: {
      animation: 'list'
    }
  },
  {
    path: ':id',
    component: ProjectDetailsComponent,
    data: {
      animation: 'details'
    }
  }
];
