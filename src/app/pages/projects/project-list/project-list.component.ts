import { Component } from '@angular/core';

// TODO: Refactor.
import { ListItem } from '../../dashboard/dashboard.component';

/**
 * List of all the projects.
 */
@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent {

  proProjects: ListItem[] = [
    {
      id: 3,
      name: 'Edycem',
      description: 'Juin 2019'
    }
  ];

  personalProjects: ListItem[] = [
    {
      id: 0,
      name: 'Portfolio',
      description: 'Octobre 2019'
    },
    {
      id: 1,
      name: 'Gameboy Pokémon',
      description: 'Septembre 2019',
      logo: '../../../assets/images/icons/mock/pokemon.svg'
    },
    {
      id: 2,
      name: 'Labyrinthe 3D',
      description: 'Août 2019',
      logo: '../../../assets/images/icons/mock/maze.svg'
    },
    {
      id: 0,
      name: 'Portfolio',
      description: 'Octobre 2019'
    },
    {
      id: 1,
      name: 'Gameboy Pokémon',
      description: 'Septembre 2019',
      logo: '../../../assets/images/icons/mock/pokemon.svg'
    },
    {
      id: 2,
      name: 'Labyrinthe 3D',
      description: 'Août 2019',
      logo: '../../../assets/images/icons/mock/maze.svg'
    },
    {
      id: 0,
      name: 'Portfolio',
      description: 'Octobre 2019'
    },
    {
      id: 1,
      name: 'Gameboy Pokémon',
      description: 'Septembre 2019',
      logo: '../../../assets/images/icons/mock/pokemon.svg'
    },
    {
      id: 2,
      name: 'Labyrinthe 3D',
      description: 'Août 2019',
      logo: '../../../assets/images/icons/mock/maze.svg'
    },
    {
      id: 0,
      name: 'Portfolio',
      description: 'Octobre 2019'
    },
    {
      id: 1,
      name: 'Gameboy Pokémon',
      description: 'Septembre 2019',
      logo: '../../../assets/images/icons/mock/pokemon.svg'
    },
    {
      id: 2,
      name: 'Labyrinthe 3D',
      description: 'Août 2019',
      logo: '../../../assets/images/icons/mock/maze.svg'
    }
  ];

  schoolProjects: ListItem[] = [
    {
      id: 3,
      name: 'Edycem',
      description: 'Juin 2019'
    }
  ];
}
