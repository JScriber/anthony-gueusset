import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { slider } from './route-animations';

// Services.
import { MenuManagerService } from './layouts/menu-manager/menu-manager.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [ slider ]
})
export class AppComponent {

  constructor(private readonly menu: MenuManagerService) {}

  get menuState$() {
    return this.menu.state$;
  }

  returnToView() {
    this.menu.changeState(false);
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }
}
