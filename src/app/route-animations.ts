import { trigger, transition, query, style, animate, group } from '@angular/animations';

// DO NOT TRY TO REFACTOR. AOT issue.
export function goToLeft() {

  return [
    query(':enter, :leave', [
      style({
        position: 'fixed',
        top: 0,
        left: 0,
        width: '100%'
      })
    ], { optional: true }),

    query(':enter', [
      style({ left: '-100%' })
    ]),

    group([
      query(':leave', [
        animate('450ms ease', style({ left: '100%' }))
      ], { optional: true }),
      query(':enter', [
        animate('450ms ease', style({ left: '0%' }))
      ])
    ])
  ];
}

// DO NOT TRY TO REFACTOR. AOT issue.
export function goToRight() {

  return [
    query(':enter, :leave', [
      style({
        position: 'fixed',
        top: 0,
        right: 0,
        width: '100%'
      })
    ], { optional: true }),

    query(':enter', [
      style({ right: '-100%' })
    ]),

    group([
      query(':leave', [
        animate('450ms ease', style({ right: '100%' }))
      ], { optional: true }),
      query(':enter', [
        animate('450ms ease', style({ right: '0%' }))
      ])
    ])
  ];
}

export const slider = trigger('routeAnimations', [
  // Dashboard to list.
  transition('dashboard => list', goToRight()),
  transition('list => dashboard', goToLeft()),

  // Dashboard to details.
  transition('dashboard => details', goToRight()),
  transition('details => dashboard', goToLeft()),

  // List to details.
  transition('list => details', goToRight()),
  transition('details => list', goToLeft())
]);
