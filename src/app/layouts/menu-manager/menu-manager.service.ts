import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class MenuManagerService {

  /** State manager. */
  private readonly state = new BehaviorSubject<boolean>(false);

  /** State access. */
  get state$() {
    return this.state;
  }

  /**
   * Changes the state of the menu.
   */
  changeState(state: boolean) {
    this.state.next(state);
  }

}
