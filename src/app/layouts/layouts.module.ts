import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';

// Components.
import { FooterComponent } from './footer/footer.component';
import { MainActionComponent } from './main-action/main-action.component';

// Services.
import { MenuManagerService } from './menu-manager/menu-manager.service';

@NgModule({
  imports: [
    CommonModule,
    MatButtonModule
  ],
  declarations: [
    FooterComponent,
    MainActionComponent
  ],
  providers: [
    MenuManagerService
  ],
  exports: [
    FooterComponent,
    MainActionComponent
  ]
})
export class LayoutsModule {}
