import { Component, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

/**
 * Searchbar.
 */
@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.scss']
})
export class SearchbarComponent implements OnDestroy {

  /**
   * Outputs the search result.
   * Returns undefined when the search is canceled.
   */
  @Output() search = new EventEmitter<string | undefined>();

  form = new FormGroup({
    input: new FormControl('', [Validators.required])
  });

  /** Input quick accessor. */
  get input() {
    return this.form.get('input');
  }

  /** Submit the research. */
  doSearch() {
    if (this.form.valid) {
      this.search.emit(this.input.value);
    }
  }

  /** Clear the input. */
  clear() {
    this.input.setValue('');
    this.search.emit();
  }

  ngOnDestroy() {
    this.search.complete();
  }

}
